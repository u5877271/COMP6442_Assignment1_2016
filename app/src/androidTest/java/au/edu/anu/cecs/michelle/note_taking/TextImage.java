package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 27/03/16.
 */

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.ImageButton;

public class TextImage extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mMainActivity;
    private Intent mIntent;
    //Constructor
    public TextImage() {

        super("au.edu.anu.cecs.michelle.note_taking", MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {//Initialize test environment
        super.setUp();
        mIntent = new Intent(getInstrumentation().getTargetContext(),MainActivity.class);
    }
    @MediumTest
    public void testButton_image(){// Test if return same image
        mMainActivity = getActivity();
        final ImageButton mButton = (ImageButton)getActivity().findViewById(R.id.text);
        final Object buttonView= getActivity().getResources().getDrawable(R.mipmap.sketchup_01);
        assertEquals("Text image",buttonView,mButton.getResources().getDrawable(R.mipmap.sketchup_01));
    }
}
