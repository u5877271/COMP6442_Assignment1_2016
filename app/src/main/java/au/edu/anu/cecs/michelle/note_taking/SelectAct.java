package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 09/03/16.
 */

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

public class SelectAct extends Activity implements View.OnClickListener {
    /* Declare widget objects */
    private Button s_delete,s_edit;
    private ImageButton shareText,shareImg;
    private ImageView s_img;
    private VideoView s_video;
    private TextView s_tv,s_title;
    /* Declare database objects */
    private NotesDB notesDB;
    private SQLiteDatabase dbWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.select);

//        System.out.println(getIntent().getIntExtra(NotesDB.ID,0));
        /* Instantiate widgets*/
        s_delete = (Button) findViewById(R.id.s_delete);
        s_edit = (Button) findViewById(R.id.s_edit);
        s_img = (ImageView) findViewById(R.id.s_img);
        s_video = (VideoView) findViewById(R.id.s_video);
        s_tv = (TextView) findViewById(R.id.s_tv);
        s_title = (TextView) findViewById(R.id.s_title);
        /* Open database writer */
        notesDB = new NotesDB(this);
        dbWriter = notesDB.getWritableDatabase();
        shareText = (ImageButton)findViewById(R.id.shareText);
        shareImg = (ImageButton)findViewById(R.id.shareImg);
        /* Set button image */
        shareText.setImageDrawable(getResources().getDrawable(R.mipmap.sharetext));
        shareImg.setImageDrawable(getResources().getDrawable(R.mipmap.shareimg));
        /* Click listener for launching button */
        shareText.setOnClickListener(this);
        shareImg.setOnClickListener(this);
        s_edit.setOnClickListener(this);
        s_delete.setOnClickListener(this);
        /* Open image , set video invisible in select.xml */
        if(getIntent().getStringExtra(NotesDB.PATH).equals("null")){
            s_img.setVisibility(View.GONE);
        }else {
            s_img.setVisibility(View.VISIBLE);
        }
        /* Open video , set image invisible in select.xml */
        if(getIntent().getStringExtra(NotesDB.VIDEO).equals("null")){
            s_video.setVisibility(View.GONE);
        }else {
            s_video.setVisibility(View.VISIBLE);
        }
        /* write different types of data into Note database */
        s_title.setText(getIntent().getStringExtra(NotesDB.TITLE));
        s_tv.setText(getIntent().getStringExtra(NotesDB.CONTENT));
        Bitmap bitmap = BitmapFactory.decodeFile(getIntent().getStringExtra(NotesDB.PATH));
        s_img.setImageBitmap(bitmap);
        s_video.setVideoURI(Uri.parse(getIntent().getStringExtra(NotesDB.VIDEO)));
        s_video.start();
    }
    /* Choose which button clicked in select.xml layout */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.shareText:
                shareText();
                break;
            case R.id.shareImg:
                shareImg();
                break;
            case R.id.s_delete:
                deleteData();
                finish();
                break;
            case R.id.s_edit:
                editData();
                finish();
                break;
        }

    }
    /* Share texts to social link */
    private void shareText(){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "This is the text that will be shared.");
        startActivity(Intent.createChooser(sharingIntent,"Share Text"));
    }
    /* Share images to social link */
    private  void shareImg(){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri screenshotUri = Uri.parse(getIntent().getStringExtra(NotesDB.PATH));

        sharingIntent.setType("image/png");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        startActivity(Intent.createChooser(sharingIntent, "Share Image"));
    }
    /* edit new info into Note database */
    private void editData() {
        ContentValues cv = new ContentValues();
        cv.put(NotesDB.TITLE, s_title.getText().toString());
        cv.put(NotesDB.CONTENT, s_tv.getText().toString());
        dbWriter.update(NotesDB.TABLE_NAME, cv, "_id=" + getIntent().getIntExtra(NotesDB.ID, 0), null);
    }
    /* delete info from Note database */
    private void deleteData(){
        dbWriter.delete(NotesDB.TABLE_NAME, "_id=" + getIntent().getIntExtra(NotesDB.ID, 0), null);
    }


}
