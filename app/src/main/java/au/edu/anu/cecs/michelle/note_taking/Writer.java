package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 08/03/16.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.VideoView;

public class Writer extends Activity implements OnClickListener {

    /* Declare widget objects */
    private Button savebtn, deletebtn;
    private EditText ettext,titext;
    private ImageView c_img;
    private VideoView v_video;
    /* Declare database objects */
    private NotesDB notesDB;
    private SQLiteDatabase dbWriter;

    private String val;
    private File phoneFile, videoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.writer);
        /**/
        val = getIntent().getStringExtra("flag");
         /* Instantiate widgets*/
        savebtn = (Button) findViewById(R.id.save);
        deletebtn = (Button) findViewById(R.id.delete);
        ettext = (EditText) findViewById(R.id.ettext);
        titext= (EditText) findViewById(R.id.title);
        c_img = (ImageView) findViewById(R.id.c_img);
        v_video = (VideoView) findViewById(R.id.c_video);
        /* Click listener for launching button */
        savebtn.setOnClickListener(this);
        deletebtn.setOnClickListener(this);
        /* Open database writer */
        notesDB = new NotesDB(this);
        dbWriter = notesDB.getWritableDatabase();
        initView();
    }
    /* write different types of messages submit to database */
    public void initView() {
        if (val.equals("1")) { // text
            c_img.setVisibility(View.GONE);
            v_video.setVisibility(View.GONE);
        }
        if (val.equals("2")) {//image
            c_img.setVisibility(View.VISIBLE);
            v_video.setVisibility(View.GONE);
            Intent iimg = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            /* Write image in fixed format */
            phoneFile = new File(Environment.getExternalStorageDirectory()
                    .getAbsoluteFile() + "/" + getTime() + ".jpg");
            iimg.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(phoneFile));
            startActivityForResult(iimg, 1);
        }
        if (val.equals("3")) {//video
            c_img.setVisibility(View.GONE);
            v_video.setVisibility(View.VISIBLE);
            /* Open system camera */
            Intent video = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            /* Write video in fixed format */
            videoFile = new File(Environment.getExternalStorageDirectory()
                    .getAbsoluteFile() + "/" + getTime() + ".mp4");
            video.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));
            startActivityForResult(video, 2);
        }
    }
    /* Choose which button clicked in writer.xml layout */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                addDB();
                finish();
                break;

            case R.id.delete:
                finish();
                break;
        }
    }
    /* Insert message into database */
    public void addDB() {
        ContentValues cv = new ContentValues();
        cv.put(NotesDB.TITLE,titext.getText().toString());
        cv.put(NotesDB.CONTENT, ettext.getText().toString());
        cv.put(NotesDB.TIME, getTime());
        cv.put(NotesDB.PATH, phoneFile + "");
        cv.put(NotesDB.VIDEO, videoFile + "");
        dbWriter.insert(NotesDB.TABLE_NAME, null, cv);
    }
    /* Get system time and set date format */
    private String getTime() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date curDate = new Date();
        String str = format.format(curDate);
        return str;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {//sending image
            Bitmap bitmap = null;
            try {
                //via BitmapFactory get path
                bitmap = BitmapFactory.decodeStream(
                        getContentResolver().openInputStream(Uri.fromFile(phoneFile)));
                //via path to show image
                c_img.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        if (requestCode == 2) {
            v_video.setVideoURI(Uri.fromFile(videoFile));
            v_video.start();
        }
    }

}