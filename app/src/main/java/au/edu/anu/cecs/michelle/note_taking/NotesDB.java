package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 07/03/16.
 */
 
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class NotesDB extends SQLiteOpenHelper{
    /* Declare database constant */
    public static  final String TABLE_NAME = "notes";
    public static  final String CONTENT = "content";
    public static  final String PATH = "path";
    public static  final String VIDEO = "video";
    public static  final String ID ="_id";
    public static  final String TIME="time";
    public static  final String TITLE="title";
    /* Initialize constructor*/
    public NotesDB(Context context){

        super(context,"notes",null,1);
    }
    /* Create database table */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME+" ("
                +ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                +TITLE+" TEXT NOT NULL,"
                +CONTENT+" TEXT NOT NULL,"
                +PATH+" TEXT NOT NULL,"
                +VIDEO+" TEXT NOT NULL,"
                +TIME+" TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}