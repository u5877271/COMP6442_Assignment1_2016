package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 07/03/16.
 */
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LayoutAnimationController;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import au.edu.anu.cecs.michelle.note_taking.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    /* Declare widget objects */
    private ImageButton textbtn,imgbtn,videobtn;
    private Intent intent;
    private MyAdapter adapter;
    /* Declare database objects */
    private NotesDB notesDB;
    private SQLiteDatabase dbReader;
    private Cursor cursor;
    private GridView gridView;
    private ScaleAnimation sa;
    private LayoutAnimationController lac;
    /* Lab modification */
    private Button btn_num;
    private TextView text_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();


    }

    public void initView(){
        /* Lab modification */
        btn_num= (Button) findViewById(R.id.button_num);
        text_num= (TextView) findViewById(R.id.editText_num);
        /* Instantiate widgets*/
        gridView = (GridView) findViewById(R.id.grid);
        textbtn = (ImageButton)findViewById(R.id.text);
        imgbtn = (ImageButton)findViewById(R.id.img);
        videobtn = (ImageButton)findViewById(R.id.video);
        /* Set button image */
        textbtn.setImageDrawable(getResources().getDrawable(R.mipmap.sketchup_01));
        imgbtn.setImageDrawable(getResources().getDrawable(R.mipmap.instagram));
        videobtn.setImageDrawable(getResources().getDrawable(R.mipmap.audiobox_01));
        /* Click listener for launching button */
        textbtn.setOnClickListener(this);
        imgbtn.setOnClickListener(this);
        videobtn.setOnClickListener(this);
        /* Open database Reader */
        notesDB = new NotesDB(this);
        dbReader = notesDB.getReadableDatabase();

        /* Design grid view */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                /* Launch SelectAct activity */
                Intent i = new Intent(MainActivity.this, SelectAct.class);
                /* Move cursor to get data to put into Intent */
                i.putExtra(NotesDB.ID, cursor.getInt(cursor.getColumnIndex(NotesDB.ID)));
                i.putExtra(NotesDB.CONTENT, cursor.getString(cursor.getColumnIndex(NotesDB.CONTENT)));
                i.putExtra(NotesDB.TITLE,cursor.getString(cursor.getColumnIndex(NotesDB.TITLE)));
                i.putExtra(NotesDB.TIME, cursor.getString(cursor.getColumnIndex(NotesDB.TIME)));
                i.putExtra(NotesDB.PATH, cursor.getString(cursor.getColumnIndex(NotesDB.PATH)));
                i.putExtra(NotesDB.VIDEO, cursor.getString(cursor.getColumnIndex(NotesDB.VIDEO)));
                startActivity(i);
            }
        });
        /* Set scale Animation */
        sa = new ScaleAnimation(0,1,0,1);
        sa.setDuration(1000);
        lac = new LayoutAnimationController(sa,0.5f);
        gridView.setLayoutAnimation(lac);
    }
    /* Choose which button clicked in activity_main.xml
     * Put data into intent  */
    @Override
    public void onClick(View v) {
        intent = new Intent(this,Writer.class);
        switch (v.getId()){
            case R.id.text:
                intent.putExtra("flag","1");
                startActivity(intent);
                break;
            case R.id.img:
                intent.putExtra("flag","2");
                startActivity(intent);
                break;
            case R.id.video:
                intent.putExtra("flag","3");
                startActivity(intent);
                break;
        }
    }
    /* set adapter to display entire message */
    public void selectDB(){
        cursor = dbReader.query(NotesDB.TABLE_NAME,null,null,null,null,null, null);
        adapter = new MyAdapter(this,cursor);
        gridView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectDB();
    }
    /* Lab modification */
    public void ShowNum(View view){
            Cursor cursor = dbReader.query(NotesDB.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String ID = cursor.getString(cursor.getColumnIndex(NotesDB.ID));
                    text_num.setText(ID);
                }
                cursor.close();
            }
        }
}