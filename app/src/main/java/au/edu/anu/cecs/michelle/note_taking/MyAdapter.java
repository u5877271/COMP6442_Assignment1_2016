package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 09/03/16.
 */
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private Cursor cursor;
    private LinearLayout layout;
    /* Initialize constructor*/
    public MyAdapter(Context context, Cursor cursor) {
        this.context = context;
        this.cursor = cursor;
    }
    /* Override Cursor method*/
    @Override
    public int getCount() {
        return cursor.getCount();
    }
    /* Override Cursor method*/
    @Override
    public Object getItem(int position) {
        return cursor.getPosition();
    }
    /* Override Cursor method*/
    @Override
    public long getItemId(int position) {

        return position;
    }
    /* Override Cursor method & Instantiate layout */
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (LinearLayout) inflater.inflate(R.layout.my_layout, null);
        /* Instantiate widgets*/
        TextView titletv = (TextView) layout.findViewById(R.id.list_title);
        TextView contenttv = (TextView) layout.findViewById(R.id.list_content);
        TextView timetv = (TextView) layout.findViewById(R.id.list_time);
        ImageView imgiv = (ImageView) layout.findViewById(R.id.list_img);
        ImageView videoiv = (ImageView) layout.findViewById(R.id.list_video);
        /* Move cursor to get data type */
        cursor.moveToPosition(position);
        String title = cursor.getString(cursor.getColumnIndex("title"));
        String content = cursor.getString(cursor.getColumnIndex("content"));
        String time = cursor.getString(cursor.getColumnIndex("time"));
        String url = cursor.getString(cursor.getColumnIndex("path"));
        String urlvideo = cursor.getString(cursor.getColumnIndex("video"));
        /* Show text, image and video messages in activity_main.xml */
        titletv.setText(title);
        contenttv.setText(content);
        timetv.setText(time);
        videoiv.setImageBitmap(getVideoThumbnail(urlvideo, 200, 200,
                MediaStore.Images.Thumbnails.MICRO_KIND));
        imgiv.setImageBitmap(getImageThumbnail(url, 200, 200));
        return layout;
    }
    /* Get Image Thumbnail to display in activity_main.xml */
    public Bitmap getImageThumbnail(String uri, int width, int height) {
    //get image path, set image width and height
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //get image
        bitmap = BitmapFactory.decodeFile(uri, options);
        options.inJustDecodeBounds = false;
        //set thumbnail width and height
        int beWidth = options.outWidth / width;
        int beHeight = options.outHeight / height;
        // set boundary of image
        int be = 1;
        if (beWidth < beHeight) {
            be = beWidth;
        } else {
            be = beHeight;
        }
        if (be <= 0) {
            be = 1;
        }
        options.inSampleSize = be;
        // get the new thumbnail
        bitmap = BitmapFactory.decodeFile(uri, options);
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,
                ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        return bitmap;
    }

    /* Get video Thumbnail to display in activity_main.xml */
    private Bitmap getVideoThumbnail(String uri, int width, int height, int kind) {
        Bitmap bitmap = null;
        bitmap = ThumbnailUtils.createVideoThumbnail(uri, kind);
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,
                ThumbnailUtils.OPTIONS_RECYCLE_INPUT);

        return bitmap;
    }

}
