package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 27/03/16.
 */

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class TestText extends ActivityInstrumentationTestCase2 {
    private TextView mView;
    private String resourceString;
    //Constructor
    public TestText() {
        super("au.edu.anu.cecs.michelle.note_taking", SelectAct.class);
    }
    @Override
    protected void setUp() throws Exception {//Initialize test environment
        super.setUp();
        Activity mActivity = this.getActivity();
        mView = (TextView) mActivity.findViewById(au.edu.anu.cecs.michelle.note_taking.R.id.s_delete);
        resourceString = mActivity.getString(au.edu.anu.cecs.michelle.note_taking.R.string.Delete);
    }
    /* check whether resource string is equal to the text got from the textview */
    public void testText() {//Test String Resource
        assertEquals(resourceString,(String)mView.getText());
    }
    /*  check for initial condition to run our tests correctly.*/
    public void testPreconditions() {
        assertNotNull(mView);
    }
}
