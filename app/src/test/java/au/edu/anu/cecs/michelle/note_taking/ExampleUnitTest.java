package au.edu.anu.cecs.michelle.note_taking;
/**
 * Created by Lanmixue Mao(Michelle) on 27/03/16.
 */

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.ImageButton;
import org.junit.Test;


public class ExampleUnitTest extends ActivityUnitTestCase<MainActivity>{
    private Intent mIntent;
    private MainActivity mMainActivity;
    //Constructor
    public ExampleUnitTest() {

        super(MainActivity.class);
    }

    //JUnit Test
    @Test
    public void TestImage(){
    //Test ImageButton whether return same image
        mIntent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
        mMainActivity = getActivity();
        final ImageButton mButton = (ImageButton) getActivity().findViewById(R.id.text);
        final Object buttonView = getActivity().getResources().getDrawable(R.mipmap.sketchup_01);
        assertEquals("Text image", buttonView, mButton.getResources().getDrawable(R.mipmap.sketchup_01));

    }

}